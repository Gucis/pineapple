# Pineapple

Pineapple email subscription web page.

## Installation

Clone repo

```bash
git clone git@bitbucket.org:Gucis/pineapple.git
```

Enter project folder

```bash
cd pineapple
```

Use the dependency manager for php [composer] (https://getcomposer.org/).

```bash
composer install
```

Use javascript package manager [nodejs] (https://nodejs.org/en/).

Installs a packages
```bash
npm install
```

create database for emails

create .env file from .env.example

specify database location, user, password in .env file
(DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD)

Generate an app encryption key
```bash
php artisan key:generate
```

generate database tables
```bash
php artisan migrate
```


## Usage

start database server

start php server

```bash
php artisan serve
```
Pineapple subscription page:
http://localhost:8000/

Table containing all subscribers:
http://localhost:8000/allsubscribers
