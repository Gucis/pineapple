<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="SS_to_NL.css">  
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="SS_to_NL_media.css">
</head>


<body>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
   
    <div id="container_main">
        <div id="main_menu">
            <img id="pineapple" src="Task 1/Pineapple/logo_pineapple.svg">
            <img id="pineapple-1" src="Task 1/Pineapple/logo_pineapple-1.svg">
            
            <ul>
                <li id="About"><a href="#">About</a></li>
                <li id="Howitworks"><a href="#">How it works</a></li> 
                <li id="Contact"><a href="#">Contact</a></li>   
            
            </ul>
        </div>
        
        <div id="for_media">
            <div id="app"></div>
            <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
        </div>




    </div>
   
    <div id="Pbg">
    </div>

</body>
</html>