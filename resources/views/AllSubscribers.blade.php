<html>
    <head>
        <link rel="stylesheet" href="SS_to_NL.css"> 
        <link rel="stylesheet" href="style.css">
        <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
    </head>
    <body>
            <div id="app"></div>
            <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
        
    </body>
</html>
