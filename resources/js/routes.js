import CreateSubscriber from './components/CreateSubscriber.vue';
import ThankYou from './components/ThankYou.vue';
import AllSubscribers from './components/AllSubscribers.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: CreateSubscriber
    },
    {
        name: 'thankyou',
        path: '/thankyou',
        component: ThankYou
    },
    {
        name: 'allsubscribers',
        path: '/allsubscribers',
        component: AllSubscribers
    }
];